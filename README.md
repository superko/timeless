# Timeless

Firefox extension to hide duration info on Twitch and YouTube

## About

Some sports/competitions aren't played in strict time periods (e.g. 20 minute quarters), but are played until some number of points are scored or rounds are won.

If you're watching a recording of a match, knowing the total length of the video or seeing how much of the video remains can constitute a spoiler. This is especially true in double-elimination tournaments, where the grand final match has the possibility of a "bracket reset" which roughly doubles the length of the match.

**Timeless** allows you to turn off all video duration info on Twitch or YouTube. This includes the duration in the preview thumbnail when browsing videos, the duration on the overlay while you're watching the video, and the seek bar.

Catch up on your VODs spoiler-free!

## FAQ

### Can you still seek through a video?

With the video focused, you can press the right/left arrows to advance/retreat the video by 5 seconds at a time.

### Will you support Chrome?

Probably not.

### Will you support other video sites? Will you implement this other feature?

Put in a request via an issue on GitLab or write to `extensions@superko.org`
