/**
 * 2023-04-18
 * HACK: Twitch classnames are extremely minified, difficult to target.
 * `... tw-media-card-stat` targets duration display but is prone to break.
 */

const inlineStyles = {
  twitch: `
    div:not([class^="ScPositionCorner"]) + div[class^="ScPositionCorner"] .tw-media-card-stat,
    div[data-a-target="player-seekbar"],
    .vod-seekbar-time-labels,
    .player-root .player-slider,
    .tw-link .tw-progress-bar {
      display: none !important;
    }
  `,
  youtube: `
    #ytd-player .ytp-tooltip,
    #ytd-player .ytp-progress-bar-container,
    #ytd-player .ytp-time-separator,
    #ytd-player .ytp-time-duration,
    .ytd-thumbnail-overlay-time-status-renderer {
      display: none !important;
    }
  `,
};

function insertStyleIntoPage() {
  let styleBlock = document.getElementById("timeless-style-block");
  if (!styleBlock) {
    document.body.insertAdjacentHTML(
      "afterbegin",
      `<style
          id="timeless-style-block"
          style="
            display:block;
            visibility:hidden;
            position: absolute;
            max-height: 0;
            max-width: 0;
          "
        ></style>`
    );
    styleBlock = document.getElementById("timeless-style-block");
  }
  styleBlock.innerHTML = ["twitch", "youtube"]
    .map((site) => {
      return `${window[`hide-on-${site}`] ? inlineStyles[site] : ""}`;
    })
    .join(" ");
}

// listen for changes to storage, update window vars & style insertion when they change
// storage = source of truth
browser.storage.onChanged.addListener(onStorageChanged);
function onStorageChanged(changes) {
  const keys = Object.keys(changes);
  for (var key of keys) {
    window[key] = changes[key].newValue;
  }
  insertStyleIntoPage();
}

// On first run, need to apply the existing state, before any changes have been made.
browser.storage.local
  .get()
  .then((obj) => {
    const keys = Object.keys(obj);
    for (var key of keys) {
      window[key] = obj[key];
    }
    insertStyleIntoPage();
  })
  .catch((err) => console.error(err));
