const toggles = ["hide-on-youtube", "hide-on-twitch"];

browser.storage.local
  .get()
  .then((data) => {
    toggles.forEach((id) => {
      const el = document.getElementById(id);
      el.checked = data[id] || false;
      el.addEventListener("change", onToggle);
    });
  })
  .catch(onError);

function onToggle(ev) {
  browser.storage.local
    .set({ [ev.target.id]: ev.target.checked })
    .catch((err) => console.error(err));
}

function onError(err) {
  document.getElementById("error-message").classList.remove("hidden");
  document.getElementById("menu-content").classList.add("hidden");

  // FIX: these errors don't actually send to browser
  // they are contained to menu.html which is its own document
  console.error(`Failed to execute Timeless script: ${err.message}`);
}
